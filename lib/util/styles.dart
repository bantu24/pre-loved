// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class Styles {
  static Color primary = Colors.blue[300] as Color;
}

class TextStyleBlackBold {
  static TextStyle h1 =
      TextStyle(fontSize: 25, fontWeight: FontWeight.bold, color: Colors.black);
  static TextStyle h2 =
      TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black);
  static TextStyle h3 =
      TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black);
}
