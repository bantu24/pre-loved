import 'dart:collection';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:junction_app2021/pages/camera.dart';
import 'package:junction_app2021/pages/clothes.dart';
import 'package:junction_app2021/pages/clothing3d.dart';
import 'package:junction_app2021/pages/my_clothes.dart';
import 'package:junction_app2021/pages/settings.dart';
import 'package:junction_app2021/pages/shop.dart';
import 'package:junction_app2021/util/styles.dart';

Future<void> main() async {
  // Ensure that plugin services are initialized so that `availableCameras()`
  // can be called before `runApp()`
  WidgetsFlutterBinding.ensureInitialized();

  // Obtain a list of the available cameras on the device.
  final cameras = await availableCameras();

  // Get a specific camera from the list of available cameras.
  final firstCamera = cameras.first;

  runApp(
    MaterialApp(
      theme: ThemeData.dark(),
      debugShowCheckedModeBanner: false,
      home: Main(
        // Pass the appropriate camera to the TakePictureScreen widget.
        camera: firstCamera,
      ),
    ),
  );
}

class Main extends StatelessWidget {
  final CameraDescription camera;
  const Main({Key? key, required this.camera}) : super(key: key);
  final String title = 'Pre-Loved';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: true,
      title: 'Junction App',
      theme: ThemeData(
        primarySwatch: Colors.grey,
      ),
      routes: {
        MyHomePage.routeName: (context) => MyHomePage(title: title),
        SkeletonDetection.routeName: (context) => SkeletonDetection(),
        ImageCreate.routeName: (context) => ImageCreate(),
        ShopPage.routeName: (context) => const ShopPage(),
        TakePictureScreen.routeName: (context) =>
            TakePictureScreen(camera: camera),
      },
      home: MyHomePage(title: title),
    );
  }
}

class MyHomePage extends StatefulWidget {
  static String routeName = '/main';
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _index = 0;

  void _onItemTapped(int index) {
    if (index != _index) {
      _navigationQueue.removeWhere((element) => element == index);
      _navigationQueue.addLast(index);
      setState(() {
        _index = index;
      });
    }
  }

  Widget? currentPage;

  void reload() {
    setState(() {});
  }

  final ListQueue<int> _navigationQueue = ListQueue();
  bool _fingerPrintSwitch = false;

  bool first = true;
  @override
  Widget build(BuildContext context) {
    if (first) {
      first = false;
      SchedulerBinding.instance!.addPostFrameCallback((_) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content:
              Text("Please go to the settings and take a picture of yourself"),
        ));
      });
    }
    List<Widget> _pages = [
      mainView(context, reload),
      const MyClothesView(),
      shop(),
      const SettingPage(),
    ];

    return WillPopScope(
      onWillPop: () async {
        if (mView != null) {
          mView = null;
          reload();
          return false;
        } else {
          if (_navigationQueue.isEmpty) return true;
          setState(() {
            _navigationQueue.removeLast();
            int position = _navigationQueue.isEmpty ? 0 : _navigationQueue.last;
            _index = position;
          });
          return false;
        }
      },
      child: Scaffold(
          backgroundColor: Colors.grey[100],
          appBar: AppBar(
            title: Text(
              widget.title,
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
            centerTitle: true,
            backgroundColor: Colors.transparent,
            shadowColor: Colors.transparent,
            leading: mView == null
                ? null
                : IconButton(
                    icon: Icon(Icons.arrow_back, color: Colors.black),
                    onPressed: () => {mView = null, reload()}),
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 10),
                child:
                    Image.asset('assets/logo50px.png', width: 40, height: 40),
              ),
            ],
          ),
          bottomNavigationBar: BottomNavigationBar(
            items: <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.home, /*color: Styles.primary*/
                ),
                label: 'Home',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.person /*, color: Styles.primary*/),
                label: 'My Sales',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.shopping_bag /*, color: Styles.primary*/),
                label: 'Shop',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.settings /*, color: Styles.primary*/),
                label: 'Settings',
              ),
            ],
            currentIndex: _index,
            unselectedItemColor: Styles.primary,
            selectedItemColor: Colors.blue[900],
            selectedLabelStyle: TextStyle(fontWeight: FontWeight.bold),
            selectedIconTheme: IconThemeData(size: 30),
            iconSize: 25,
            onTap: _onItemTapped,
          ),
          body: IndexedStack(
            index: _index,
            children: _pages,
          )),
    );
  }
}

Widget? mView;

Widget piece(BuildContext context, Clothes clothes, Function f) {
  return Padding(
    padding: const EdgeInsets.only(bottom: 20),
    child: FittedBox(
      fit: BoxFit.scaleDown,
      child: InkWell(
        onTap: () {
          mView = mainClothes(context, clothes);
          f();
          //Navigator.pushNamed(context, '/clothes', arguments: clothes);
        },
        child: Card(
          elevation: 1,
          shadowColor: Colors.black,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(40),
          ),
          child: Container(
            height: 480,
            width: 400,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(40),
              image: DecorationImage(
                colorFilter: ColorFilter.mode(
                    Colors.black.withOpacity(0.05), BlendMode.srcOver),
                image: NetworkImage(clothes.images.first),
                fit: BoxFit.fitWidth,
                alignment: Alignment.center,
              ),
            ),
            padding: const EdgeInsets.all(10),
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.topLeft,
                  child: Container(
                    width: 150,
                    child: ListTile(
                      title: Row(
                        children: [
                          Flexible(child: Text(clothes.name)),
                        ],
                      ),
                      subtitle: Text(
                        clothes.type,
                        style: TextStyle(color: Colors.black.withOpacity(0.6)),
                      ),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 15, bottom: 15),
                    child: Text(
                      '${clothes.price} €',
                      style: TextStyleBlackBold.h2,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    ),
  );
}

Widget mainView(BuildContext context, Function f) {
  f();
  Clothes clothes = Clothes([
    'https://ih1.redbubble.net/image.972400358.3607/ssrco,slim_fit_t_shirt,two_model,fafafa:ca443f4786,front,tall_portrait,750x1000.jpg'
  ], 'https://www.marimekko.com/fi_fi/vaatteet/kaikki-tuotteet/ohikulkeva-kivet-tunika-t-sininen-musta-090400-059',
      235.00, 'Ohikulkeva Kivet', 'Tunika', ClothingToTest('kapinaRed'));

  List<Clothes> list = [
    Clothes([
      'https://www.marimekko.com/media/catalog/product/0/9/090231-003_X191757_10_1632828091.jpg?width=1120&height=1400&canvas=1120:1400&quality=100'
    ], 'https://www.marimekko.com/fi_fi/vaatteet/paidat-ja-tunikat/kapina-suur-unikko-2-t-paita-punainen-tumma-punainen-090231-003',
        105.00, 'Kapina Suur Unikko 2', 'T-paita', ClothingToTest('kapinaRed')),
    Clothes([
      'https://drive.google.com/uc?id=1CnMETyeddOaGn310B4gQ5dFZbl0XCEtB'
    ], 'https://www.marimekko.com/fi_fi/vaatteet/paidat-ja-tunikat/kapina-suur-unikko-2-t-paita-punainen-tumma-punainen-090231-003',
        1, 'Junction', 'T-paita', ClothingToTest('junction')),
    Clothes([
      'https://ih1.redbubble.net/image.972400358.3607/ssrco,slim_fit_t_shirt,two_model,fafafa:ca443f4786,front,tall_portrait,750x1000.jpg'
    ], 'https://www.marimekko.com/fi_fi/vaatteet/paidat-ja-tunikat/vaikutus-unikko-t-paita-valkoinen-t-sininen-090337-015',
        30, 'Huawei', 'ML', ClothingToTest('huaweiShirt')),
    Clothes([
      'https://www.marimekko.com/media/catalog/product/0/9/090231-002_X191668_10_1634959550.jpg?width=1120&height=1400&canvas=1120:1400&quality=100'
    ],
        'https://www.marimekko.com/fi_fi/vaatteet/paidat-ja-tunikat/kapina-suur-unikko-2-t-paita-beige-kulta-090231-002',
        105.00,
        'Kapina Suur Unikko 2',
        'T-paita',
        ClothingToTest('kapinaYellow')),
    Clothes([
      'https://www.marimekko.com/media/catalog/product/0/9/090289-910_X191745_20_1630122046.jpg?width=1120&height=1400&canvas=1120:1400&quality=100&bg-color=255,255,255&fit=bounds'
    ],
        'https://www.marimekko.com/fi_fi/vaatteet/paidat-ja-tunikat/hiekka-unikko-placement-t-paita-musta-l-valkoinen-090289-910',
        95.00,
        'Hiekka Unikko Placement',
        'T-paita',
        ClothingToTest('blackShirt')),
    Clothes([
      'https://www.marimekko.com/media/catalog/product/0/9/090235-192_X191845_10_1632833535.jpg?width=1120&height=1400&canvas=1120:1400&quality=100&bg-color=255,255,255&fit=bounds'
    ], 'https://www.marimekko.com/fi_fi/vaatteet/paidat-ja-tunikat/hyvike-maisema-unikko-t-paita-l-valkoinen-harmaat-pinkki-090235-192',
        100.00, 'Hyvike Maisema Unikko', 'T-paita', ClothingToTest('kapinaRed')),
    Clothes([
      'https://www.marimekko.com/media/catalog/product/0/9/090045-690_X185434_10_1621656074.jpg?width=1120&height=1400&canvas=1120:1400&quality=100&bg-color=255,255,255&fit=bounds'
    ],
        'https://www.marimekko.com/fi_fi/hiekka-unikko-placement-t-paita-v-vihrea-musta-090045-690',
        95.00,
        'Hiekka Unikko Placement',
        'T-paita',
        ClothingToTest('kapinaRed')),
    Clothes([
      'https://www.marimekko.com/media/catalog/product/0/9/090047-068_X185635_10_1623853247.jpg?width=1120&height=1400&canvas=1120:1400&quality=100&bg-color=255,255,255&fit=bounds'
    ], 'https://www.marimekko.com/fi_fi/vaatteet/paidat-ja-tunikat/lyhythiha-logo-placement-paita-valkoinen-musta-beige-090047-068',
        95.00, 'Lyhythiha Logo Placement', 'Paita', ClothingToTest('kapinaRed')),
    Clothes([
      'https://www.marimekko.com/media/catalog/product/0/4/044734-070_X192914_10_1631855497.jpg?width=1120&height=1400&canvas=1120:1400&quality=100&bg-color=255,255,255&fit=bounds'
    ], 'https://www.marimekko.com/fi_fi/vaatteet/paidat-ja-tunikat/lyhythiha-paita-valkoinen-sininen-044734-070',
        60.00, 'Lyhythiha', 'Paita', ClothingToTest('kapinaRed')),
    Clothes([
      'https://www.marimekko.com/media/catalog/product/0/9/090030-189_X185567_10_1624515669.jpg?width=1120&height=1400&canvas=1120:1400&quality=100&bg-color=255,255,255&fit=bounds'
    ],
        'https://www.marimekko.com/fi_fi/vaatteet/paidat-ja-tunikat/toiveikas-pieni-unikko-2-paita-hiekka-ruskea-l-valkoinen-090030-189',
        205.00,
        'Toiveikas Pieni Unikko 2',
        'Paita',
        ClothingToTest('kapinaRed')),
    Clothes([
      'https://www.marimekko.com/media/catalog/product/0/9/090287-068_X191762_10_1630122032_1.jpg?width=1120&height=1400&canvas=1120:1400&quality=100&bg-color=255,255,255&fit=bounds'
    ], 'https://www.marimekko.com/fi_fi/vaatteet/paidat-ja-tunikat/palho-tasaraita-paita-valkoinen-musta-beige-090287-068',
        135.00, 'Palho Tasaraita', 'Paita', ClothingToTest('kapinaRed')),
  ];
  return Padding(
    padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
    child: mView ??
        ListView(
          children: [
            for (Clothes child in list) piece(context, child, f),
          ],
        ),
  );
}
