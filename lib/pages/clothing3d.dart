import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:huawei_ml/huawei_ml.dart';
import 'package:huawei_ml/skeleton/ml_skeleton_analyzer.dart';
import 'package:huawei_ml/skeleton/ml_skeleton_analyzer_setting.dart';
import 'package:image_picker/image_picker.dart';
import 'package:junction_app2021/pages/clothes.dart';
import 'package:junction_app2021/util/styles.dart';
import 'package:junction_app2021/util/util.dart';
import 'package:path_provider/path_provider.dart';
import 'package:junction_app2021/util/globals.dart' as globals;

const int width = 450;
const int height = 450;

class SkeletonDetection extends StatefulWidget {
  static String routeName = '/ml';
  @override
  _SkeletonDetectionState createState() => _SkeletonDetectionState();
}

class _SkeletonDetectionState extends State<SkeletonDetection> {
  Future<Img?> f(String path) async {
    File img = File(path);
    var decodedImage = await decodeImageFromList(img.readAsBytesSync());
    Size size =
        Size(decodedImage.width.toDouble(), decodedImage.height.toDouble());

    MLSkeletonAnalyzer analyzer = MLSkeletonAnalyzer();

    MLSkeletonAnalyzerSetting setting = MLSkeletonAnalyzerSetting();
    setting.path = path;
    path = path;
    setting.analyzerType = MLSkeletonAnalyzerSetting.TYPE_NORMAL;

    List<MLSkeleton> list = await analyzer.asyncSkeletonDetection(setting);

    List<MLJoint> joints = list[0].joints;

    resizeX(dynamic x) {
      return x * width / size.width;
    }

    resizeY(dynamic y) {
      return y * height / size.height;
    }

    Offset neck = Offset.zero;
    Offset leftShoulder = Offset.zero;
    Offset rightShoulder = Offset.zero;
    Offset leftHip = Offset.zero;
    Offset rightHip = Offset.zero;
    List<Offset> result = [];
    [
      109,
      108,
      108,
      107,
      107,
      110,
      110,
      111,
      111,
      112,
      106,
      105,
      105,
      104,
      104,
      114,
      114,
      101,
      101,
      102,
      102,
      103,
      113,
      114
    ].forEach((x) {
      var a = joints.firstWhere((element) => element.type == x);
      result.add(Offset(resizeX(a.pointX), resizeY(a.pointY)));
    });
    for (MLJoint joint in joints) {
      var point = Offset(resizeX(joint.pointX), resizeY(joint.pointY));
      switch (joint.type) {
        case MLJoint.TYPE_NECK:
          neck = point;
          break;
        case MLJoint.TYPE_LEFT_SHOULDER:
          leftShoulder = point;
          break;
        case MLJoint.TYPE_RIGHT_SHOULDER:
          rightShoulder = point;
          break;
        case MLJoint.TYPE_LEFT_HIP:
          leftHip = point;
          break;
        case MLJoint.TYPE_RIGHT_HIP:
          rightHip = point;
          break;
      }
      //result.add(point);
    }
    Offset middle = Offset((rightHip.dx + leftHip.dx) / 2, rightHip.dy);
    result.addAll([
      middle,
      neck,
    ]);

    bool res = await analyzer.stopSkeletonDetection();
    return Img(size, path, result, neck, leftShoulder, rightShoulder, leftHip,
        rightHip);
  }

  Future<Img?> function() async {
    final ImagePicker _picker = ImagePicker();
    // Pick an image
    Directory? appDocDir = await getExternalStorageDirectory();
    String appDocPath = appDocDir!.path;
    String path = '$appDocPath/person.png';
    //await File(image.path).copy('$appDocPath/person.png');
    //final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    try {
      if (globals.useGallery) {
        throw Exception('Ei jaksa tehdä paremmin');
      }
      var result = await f(path);
      return result;
    } catch (e) {
      try {
        final XFile? image =
            await _picker.pickImage(source: ImageSource.gallery);
        var result = await f(image!.path);
        return result;
      } catch (e) {
        return null;
      }
    }
  }

  double _currentSliderValue = 20;
  bool called = false;
  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as ClothingToTest;
    return Scaffold(
      appBar: AppBar(
        shadowColor: Colors.transparent,
        backgroundColor: Colors.transparent,
        title: const Text('Try on'),
      ),
      body: Center(
        child: FutureBuilder<Img?>(
            future: function(),
            builder: (BuildContext context, AsyncSnapshot<Img?> snapshot) {
              if (snapshot.hasData) {
                Img data = snapshot.data!;
                Image i = Image.file(File(data.path));
                print("data ${data.path} ${data.size} ${data.list}");
                List<Offset> list = data.list;
                //list.sort((a, b) => a.dy.compareTo(b.dy));
                //Offset secondHighest = list[list.length - 2];
                double min = list.reduce((a, b) => a.dx < b.dx ? a : b).dx;
                double max = list.reduce((a, b) => a.dx > b.dx ? a : b).dx;
                double center = (max + min) / 2;
                double middle = ((center + data.neck.dx) / 2).abs();
                double w =
                    (data.rightShoulder.dx - data.leftShoulder.dx).abs() * 1.2;
                double h = (((data.neck.dy - data.leftHip.dy) +
                                (data.neck.dy - data.rightHp.dy)) /
                            2)
                        .abs() *
                    1.2;
                print(
                    "width $w height $h middle $center max $max min $min highest high ${list.last} neck ${data.neck} $list");
                if (!called) {
                  called = true;
                  SchedulerBinding.instance!.addPostFrameCallback((_) {
                    Navigator.pushNamed(context, ImageCreate.routeName,
                        arguments: ImageArgs(data.path, list, middle,
                            data.leftShoulder.dy, w, h, args.name));
                  });
                }

                return Text('Loading');
              } else {
                return Text('Loading');
              }
            }),
      ),
    );
  }
}

class Img {
  final Size size;
  final String path;
  final List<Offset> list;
  final Offset neck;
  final Offset leftShoulder;
  final Offset rightShoulder;
  final Offset leftHip;
  final Offset rightHp;
  Img(this.size, this.path, this.list, this.neck, this.leftShoulder,
      this.rightShoulder, this.leftHip, this.rightHp);
}

class ImageCreate extends StatefulWidget {
  static String routeName = '/image';
  @override
  _ImageCreateState createState() => _ImageCreateState();
}

class _ImageCreateState extends State<ImageCreate> {
  String size(int value) {
    if (value == 1)
      return 'XS';
    else if (value == 2)
      return 'S';
    else if (value == 3)
      return 'M';
    else if (value == 4)
      return 'L';
    else
      return 'XL';
  }

  double getSize(int value) {
    if (value == 1)
      return 0.8;
    else if (value == 2)
      return 0.9;
    else if (value == 3)
      return 1;
    else if (value == 4)
      return 1.1;
    else
      return 1.2;
  }

  double _currentSliderValue = 3;
  bool toggleML = false;
  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as ImageArgs;
    double w = args.width * getSize(_currentSliderValue.toInt());
    double h = args.height * getSize(_currentSliderValue.toInt());
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          title: Text('Try on'),
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              onPressed: () =>
                  {Navigator.pushReplacementNamed(context, '/main')}),
        ),
        body: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FittedBox(
                  fit: BoxFit.fitWidth,
                  child: Center(
                    child: Stack(
                      children: [
                        Container(
                            width: width.toDouble(),
                            height: height.toDouble(),
                            child: Image.file(File(args.path))),
                        Positioned(
                            left: args.left - w / 2,
                            top: args.top -
                                35 * getSize(_currentSliderValue.toInt()),
                            child: ClipRRect(
                                borderRadius: BorderRadius.circular(30),
                                child: Image.asset(
                                  'assets/${args.clothingName}.png',
                                  width: w,
                                  height: h,
                                ))
                            /* Align(
                                alignment: Alignment(-100, highest.dy),
                                child: Image.asset('assets/shirtHuawei.png'),
                              )*/
                            ),
                        if (toggleML)
                          CustomPaint(
                            painter: MyPainter(args.list),
                            //                       <-- CustomPaint widget
                            size: Size(width.toDouble(), height.toDouble()),
                          )
                      ],
                    ),
                  )),
              Slider(
                value: _currentSliderValue,
                min: 1,
                max: 5,
                divisions: 4,
                thumbColor: Colors.blue,
                activeColor: Colors.blue,
                label: size(_currentSliderValue.toInt()),
                onChanged: (double value) {
                  setState(() {
                    _currentSliderValue = value;
                  });
                },
              ),
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      primary: Styles.primary,
                      textStyle: const TextStyle(fontSize: 20)),
                  onPressed: () {
                    setState(() {
                      toggleML = !toggleML;
                    });
                  },
                  child: Text(
                    'Proof of machine learning',
                    style: TextStyle(color: Colors.white),
                  ))
            ]));
  }
}

class ImageArgs {
  final String path;
  final List<Offset> list;
  final double left;
  final double top;
  final double width;
  final double height;
  final String clothingName;

  ImageArgs(this.path, this.list, this.left, this.top, this.width, this.height,
      this.clothingName);
}
