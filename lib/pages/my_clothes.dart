// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:junction_app2021/util/styles.dart';
import '../components/cloth_section.dart';

const obj = [
  {
    'id': '#10203',
    "date": '20.11.2021 10:00',
    'status': 'On Sale',
    'item': 'Hat',
    'condition': 'As New',
    'price': '48e',
    'image':
        'https://www.marimekko.com/media/catalog/product/0/9/090110-894_X185124_10_1625685912.jpg?width=1120&height=1400&canvas=1120:1400&quality=100&bg-color=255,255,255&fit=bounds'
  },
  {
    'id': '#22346',
    "date": '20.11.2021 12:00',
    'status': 'Sold',
    'item': 'Dress',
    'condition': 'Good',
    'price': '32e',
    'image':
        'https://www.marimekko.com/media/catalog/product/0/9/090034-189_X185609_10_1623657553.jpg?width=1120&height=1400&canvas=1120:1400&quality=100&bg-color=255,255,255&fit=bounds'
  },
  {
    'id': '#44123',
    "date": '20.11.2021 15:00',
    'status': 'Sold',
    'item': 'Pants',
    'condition': 'As New',
    'price': '25e',
    'image':
        'https://www.marimekko.com/media/catalog/product/0/9/090245-099_X213175_10_1632216777.jpg?width=1120&height=1400&canvas=1120:1400&quality=100&bg-color=255,255,255&fit=bounds'
  },
  {
    'id': '#53889',
    "date": '20.11.2021 15:44',
    'status': 'Sold',
    'item': 'T-Shirt',
    'condition': 'Good',
    'price': '18e',
    'image':
        'https://www.marimekko.com/media/catalog/product/0/9/090289-310_X213171_10_1632203200.jpg?width=1120&height=1400&canvas=1120:1400&quality=100&bg-color=255,255,255&fit=bounds'
  },
  {
    'id': '#98732',
    "date": '19.11.2021 12:32',
    'status': 'On Sale',
    'item': 'Bag',
    'condition': 'Good',
    'price': '36e',
    'image':
        'https://www.marimekko.com/media/catalog/product/0/4/048821-900_X146960_10_1598368464.jpg?width=1120&height=1400&canvas=1120:1400&quality=100&bg-color=255,255,255&fit=bounds'
  },
  // Add more items!
];

class MyClothesView extends StatefulWidget {
  const MyClothesView({Key? key}) : super(key: key);
  @override
  State<MyClothesView> createState() => _MyClothesState();
}

// Asks for code to show own clothes that are on sale
class _MyClothesState extends State<MyClothesView> {
  bool codeInserted = false;
  @override
  Widget build(BuildContext context) {
    if (!codeInserted) {
      return Center(
          child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset('assets/marimekko_logo_black.png'),
              SizedBox(height: 15),
              Text(
                'Write the received Package ID',
                style: TextStyle(fontSize: 17),
              ),
              Text(
                'to view your clothes:',
                style: TextStyle(fontSize: 17),
              ),
              TextFormField(
                decoration: const InputDecoration(
                  icon: Icon(Icons.archive_outlined),
                  hintText: '12345',
                  labelText: 'Package ID',
                ),
                onChanged: (codeInserted) => {true},
              ),
              SizedBox(height: 15),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  primary: Styles.primary,
                  fixedSize: const Size(180, 50),
                ),
                onPressed: () {
                  setState(() {
                    codeInserted = !codeInserted;
                  });
                },
                child: const Text('Submit',
                    style: TextStyle(fontSize: 18, color: Colors.white)),
              ),
            ]),
      ));
      // codeInserted changed to true and displaying clothes
    } else {
      return Scaffold(
          /*  appBar: AppBar(
            backgroundColor: Styles.primary,
            title: Text('My Sales'),
          ),*/
          body: ListView(
        children: [
          Container(
            decoration: BoxDecoration(
              color: Colors.grey[300],
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text('Totals:',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 24)),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text('Items Sold:',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 14)),
                        Text('3', style: TextStyle(fontSize: 14))
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text('Items on Sale:',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 14)),
                        Text('2', style: TextStyle(fontSize: 14))
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text('Revenue:',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 14)),
                        Text('75e', style: TextStyle(fontSize: 14))
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          const MyStatefulWidget(),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(80, 8, 80, 8),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                primary: Styles.primary,
                fixedSize: const Size(180, 50),
              ),
              onPressed: () {
                setState(() {
                  codeInserted = !codeInserted;
                });
              },
              child: Text(
                'Add new package ID',
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
        ],
      ));
    }
  }
}

// Could be coded better xD
List<Item> generateItems(int numberOfItems) {
  return List<Item>.generate(numberOfItems, (int index) {
    return Item(
      itemNumber: index,
    );
  });
}

/// This is the stateful widget that the main application instantiates.
class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  final List<Item> _data = generateItems(obj.length);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: _buildPanel(),
      ),
    );
  }

  Widget _buildPanel() {
    return ExpansionPanelList(
      animationDuration: Duration(seconds: 1),
      expansionCallback: (int index, bool isExpanded) {
        setState(() {
          _data[index].isExpanded = !isExpanded;
        });
      },
      children: _data.map<ExpansionPanel>((Item item) {
        return ExpansionPanel(
          backgroundColor: Color(0xfff5f5f5),
          canTapOnHeader: true,
          headerBuilder: (BuildContext context, bool isExpanded) {
            return Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(obj[item.itemNumber]["id"].toString()),
                    ),
                    Text(obj[item.itemNumber]["date"].toString())
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Padding(
                      padding:
                          obj[item.itemNumber]["status"].toString() == 'Sold'
                              ? const EdgeInsets.fromLTRB(8, 8, 70, 8)
                              : const EdgeInsets.fromLTRB(8, 8, 50, 8),
                      child: Text('Status: '),
                    ),
                    Row(
                      children: [
                        Text(obj[item.itemNumber]["status"].toString()),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(3, 3, 3, 3),
                          child: Container(
                            width: 10.0,
                            height: 10.0,
                            decoration: BoxDecoration(
                              border:
                                  Border.all(width: 1.0, color: Colors.black),
                              color:
                                  obj[item.itemNumber]["status"].toString() ==
                                          'Sold'
                                      ? Colors.green
                                      : Colors.yellow,
                              shape: BoxShape.circle,
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ],
            );
          },
          body: clothesSection(
              obj[item.itemNumber]["id"],
              obj[item.itemNumber]["date"],
              obj[item.itemNumber]["status"],
              obj[item.itemNumber]["item"],
              obj[item.itemNumber]["condition"],
              obj[item.itemNumber]["price"],
              obj[item.itemNumber]["image"]),
          isExpanded: item.isExpanded,
        );
      }).toList(),
    );
  }
}

// stores ExpansionPanel state information
class Item {
  Item({
    this.isExpanded = false,
    this.itemNumber = 0,
  });

  int itemNumber;
  bool isExpanded;
}

class Cloth {
  final String productNumber;
  final double price;
  final int condition;

  Cloth(this.productNumber, this.price, this.condition);
}

class ClothOffer {
  final String offerNumber;
  final String status;
  final double price;
  final List<Cloth> clothes;
  final String dateUpdated;

  ClothOffer(this.offerNumber, this.status, this.price, this.clothes,
      this.dateUpdated);
}
