// ignore_for_file: prefer_const_constructors

import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class ShopPage extends StatefulWidget {
  static String routeName = '/shop';
  const ShopPage({Key? key}) : super(key: key);
  @override
  State<ShopPage> createState() => _ShopState();
}

class _ShopState extends State<ShopPage> {
  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments;
    return Scaffold(body: shop(args: args));
  }
}

Widget shop({args}) {
  if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  WebViewController? _controller;
  return Padding(
      padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
      child: WebView(
          initialUrl: args ?? 'https://www.marimekko.com/fi_fi/',
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (WebViewController webViewController) {
            _controller = webViewController;
            print("web view created");
          },
          onPageFinished: (String url) {
            print('web page finished ${_controller != null}');
            if (args != null) {
              _controller!.runJavascript(
                  "let div = document.createElement('div'); div.innerHTML = '<button style=\"width:300px; height:50px\" type=\"submit\" title=\"Osta käytettynä \" class=\"button--size-medium action tocart\" id=\"product-addtocart-button\">Osta käytettynä</button>'; document.querySelector('#product_addtocart_form > div.product-options-bottom > div.box-tocart.box-tocart.typo--spacing-20---l-up').append(div);");
            }
          }));
}
