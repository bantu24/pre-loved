// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/cupertino.dart';
import 'package:junction_app2021/util/styles.dart';
import 'package:flutter/material.dart';

class ClothingToTest {
  final String name;
  ClothingToTest(this.name);
}

class Clothes {
  final List<String> images;
  final String url;
  final double price;
  final String name;
  final String type;
  final ClothingToTest clothingName;

  Clothes(this.images, this.url, this.price, this.name, this.type,
      this.clothingName);
}

Widget mainClothes(BuildContext context, Clothes args) {
  return Padding(
    padding: const EdgeInsets.only(left: 10, right: 10),
    child: ListView(
      children: [
        Image.network(args.images.first),
        ListTile(
          title: Text(args.name),
          subtitle: Text(args.type),
          trailing: IconButton(
            iconSize: 40,
            icon: Icon(Icons.threed_rotation),
            onPressed: () => {
              Navigator.pushNamed(context, '/ml', arguments: args.clothingName)
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20),
          child: Text(
            '${args.price}€',
            style: TextStyleBlackBold.h3,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(15),
          child: ElevatedButton(
            child: Text("Buy",
                style: TextStyle(fontSize: 14, color: Colors.white)),
            style: ElevatedButton.styleFrom(
              primary: Colors.black,
            ),
            onPressed: () =>
                {Navigator.pushNamed(context, '/shop', arguments: args.url)},
          ),
        ),
      ],
    ),
  );
}
