// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/cupertino.dart';
import 'package:junction_app2021/util/styles.dart';
import 'package:flutter/material.dart';
import 'package:language_picker/language_picker.dart';
import 'package:language_picker/languages.dart';
import 'package:junction_app2021/util/globals.dart' as globals;

//Color test = Styles.primary;

class SettingPage extends StatefulWidget {
  const SettingPage({Key? key}) : super(key: key);
  @override
  State<SettingPage> createState() => _SettingState();
}

class _SettingState extends State<SettingPage> {
  bool _fingerPrintSwitch = false;
  bool _privateMessageSwitch = false;
  bool _saleSwitch = false;
  bool _recommendationSwitch = false;
  bool _newProductSwitch = false;
  bool _cookieSwitch = false;
  bool _gallerySwitch = false;
  // String _address = 'Ekonominaukio 1';
  String _city = 'Espoo';
  // String _postalCode = '02150';
  String _email = 'erneste@gmail.com';
  String _userName = 'Ernest Example';
  Language _selectedDropdownLanguage = Languages.english;
  Color testcolor = Colors.grey.shade300;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
      child: ListView(
        children: [
          Text(
            'Security',
            style: TextStyleBlackBold.h2,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Container(
              decoration: BoxDecoration(
                  color: testcolor,
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: Column(
                  //padding: const EdgeInsets.fromLTRB(12, 0, 8, 0),

                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(Icons.fingerprint),
                        ),
                        Switch(
                            value: _fingerPrintSwitch,
                            onChanged: (value) {
                              setState(() {
                                _fingerPrintSwitch = value;
                              });
                            }),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text('Allow Cookies'),
                        ),
                        Switch(
                            value: _cookieSwitch,
                            onChanged: (value) {
                              setState(() {
                                _cookieSwitch = value;
                              });
                            }),
                      ],
                    ),
                  ]),
              //vanha:
              /*Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Icon(Icons.fingerprint),
                    Switch(
                        value: _fingerPrintSwitch,
                        onChanged: (value) {
                          setState(() {
                            _fingerPrintSwitch = value;
                          });
                        }),
                  ],
                ),
              ),*/
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Text(
              'Account',
              style: TextStyleBlackBold.h2,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Container(
              decoration: BoxDecoration(
                  color: testcolor,
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: Column(
                  //padding: const EdgeInsets.fromLTRB(12, 0, 8, 0),

                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text('Name'),
                        ),
                        Flexible(fit: FlexFit.tight, child: SizedBox()),
                        Expanded(
                          // Change to focus to the right border to ensure acceptable behaviour
                          child: TextFormField(
                            autofocus: false,
                            style: TextStyle(fontSize: 14),
                            initialValue: _userName,
                            onChanged: (text) => {_userName = text},
                            decoration: InputDecoration(
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8.0, 8, 10, 8),
                          child: Text('Email'),
                        ),
                        Flexible(fit: FlexFit.tight, child: SizedBox()),
                        Expanded(
                          // Change to focus to the right border to ensure acceptable behaviour
                          child: TextFormField(
                            autofocus: false,
                            style: TextStyle(fontSize: 14),
                            initialValue: _email,
                            onChanged: (text) => {_email = text},
                            decoration: InputDecoration(
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8.0, 8, 22, 8),
                          child: Text('City'),
                        ),
                        Flexible(fit: FlexFit.tight, child: SizedBox()),
                        Expanded(
                          // Change to focus to the right border to ensure acceptable behaviour
                          child: TextFormField(
                            autofocus: false,
                            style: TextStyle(fontSize: 14),
                            initialValue: _city,
                            onChanged: (text) => {_city = text},
                            decoration: InputDecoration(
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                      ],
                    ),
                    /*Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                          decoration: BoxDecoration(
                              color: Colors.grey[850],
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                            child: Text('Change Password',
                                style: (TextStyle(
                                    color: Colors.white, fontSize: 20))),
                          )),
                    ),*/
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 0, 0, 4),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            primary: Styles.primary,
                            textStyle: const TextStyle(fontSize: 20)),
                        onPressed: () {},
                        child: Text('Change Password',
                            style:
                                (TextStyle(color: Colors.white, fontSize: 20))),
                      ),
                    ),
                  ]),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Text(
              'Language',
              style: TextStyleBlackBold.h2,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Container(
              decoration: BoxDecoration(
                  color: testcolor,
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(12, 0, 8, 0),
                child: LanguagePickerDropdown(
                  initialValue: _selectedDropdownLanguage,
                  //itemBuilder: _buildDropdownItem,
                  onValuePicked: (Language language) {
                    _selectedDropdownLanguage = language;
                  },
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Text(
              'Notifications',
              style: TextStyleBlackBold.h2,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20, bottom: 10),
            child: Container(
              decoration: BoxDecoration(
                  color: testcolor,
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: Column(
                  //padding: const EdgeInsets.fromLTRB(12, 0, 8, 0),

                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text('Private Messages'),
                        ),
                        Switch(
                            value: _privateMessageSwitch,
                            onChanged: (value) {
                              setState(() {
                                _privateMessageSwitch = value;
                              });
                            }),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text('Sales'),
                        ),
                        Switch(
                            value: _saleSwitch,
                            onChanged: (value) {
                              setState(() {
                                _saleSwitch = value;
                              });
                            }),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text('Recommendations'),
                        ),
                        Switch(
                            value: _recommendationSwitch,
                            onChanged: (value) {
                              setState(() {
                                _recommendationSwitch = value;
                              });
                            }),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text('New Products'),
                        ),
                        Switch(
                            value: _newProductSwitch,
                            onChanged: (value) {
                              setState(() {
                                _newProductSwitch = value;
                              });
                            }),
                      ],
                    ),
                  ]),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Text(
              'Pictures',
              style: TextStyleBlackBold.h2,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20, bottom: 10),
            child: Container(
              decoration: BoxDecoration(
                  color: testcolor,
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: Column(
                  //padding: const EdgeInsets.fromLTRB(12, 0, 8, 0),

                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text('Import Pictures from Gallery'),
                        ),
                        Switch(
                            value: _gallerySwitch,
                            onChanged: (value) {
                              setState(() {
                                _gallerySwitch = value;
                                globals.useGallery = _gallerySwitch;
                              });
                            }),
                      ],
                    ),
                  ]),
            ),
          ),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                primary: Styles.primary,
                textStyle: const TextStyle(fontSize: 20)),
            onPressed: () {
              Navigator.of(context).pushNamed('/camera');
            },
            child: Text('Take a new photo',
                style: (TextStyle(color: Colors.white, fontSize: 20))),
          ),
        ],
      ),
    );
  }
}
